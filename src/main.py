import json
import sys

from flask import Flask
from flask import make_response

from flask_cors import CORS
from flask_cors import cross_origin

from database import Database

app = Flask(__name__)

cors = CORS(app)
app.config['CORS HEADERS'] = 'Content-type'

master_credentials = Database("../config.json")

@app.errorhandler(404)
@cross_origin()
def not_found(error):
    return make_response({'error': 'endpoint is not valid'}) # make_response is used for error handling and standardization


@app.route('/ingredients', methods = {'GET'})
@cross_origin()
def select_all_ingredients():
    connection = master_credentials.connect()
    cursor = connection.cursor()

    try:
        cursor.execute("SELECT * FROM ingredients;")
    except mariadb.Error:
        print(f'Unable to pull from ingredients table')

    data = []

    for name, m_stock, c_stock, cost, time in cursor:
        data.append({
            'ingredient_name': name,
            'max_stock': m_stock,
            'current_stock': c_stock,
            'cost': cost,
            'time_log': time
        })

    connection.close()
    return data


@app.route('/ingredients/<string:name>', methods = {'GET'})
@cross_origin()
def select_ingredient(name):
    connection = master_credentials.connect()
    cursor = connection.cursor()

    try:
        cursor.execute("SELECT * FROM ingredients WHERE ingredient_name = ?;", (name,))
    except mariadb.Error:
        print(f'Unable to pull from ingredients table')

    data = []

    for name_l, m_stock, c_stock, cost, time in cursor:
        data.append({
            'ingredient_name': name_l,
            'max_stock': m_stock,
            'current_stock': c_stock,
            'cost': cost,
            'time_log': time
        })

    connection.close()
    return data


@app.route('/ingredients_used', methods = {'GET'})
@cross_origin()
def select_all_used():
    connection = master_credentials.connect()
    cursor = connection.cursor()

    try:
        cursor.execute("SELECT * FROM ingredients_used;")
    except mariadb.Error:
        print(f'Unable to pull from used ingredients table')

    data = []

    for name, start, end, used in cursor:
        data.append({
            'ingredient_name': name,
            'start_time': start,
            'end_time': end,
            'stock_used': used
        })

    connection.close()
    return data


@app.route('/ingredients_used/<string:name>', methods = {'GET'})
@cross_origin()
def select_used_ingredient(name):
    connection = master_credentials.connect()
    cursor = connection.cursor()

    try:
        cursor.execute("SELECT * FROM ingredients_used WHERE ingredient_name = ?;",
                       (name,))
    except mariadb.Error:
        print(f'Unable to pull from used ingredients table')

    data = []

    for name_l, start, end, used in cursor:
        data.append({
            'ingredient_name': name_l,
            'start_time': start,
            'end_time': end,
            'stock_used': used
        })

    connection.close()
    return data


@app.route('/wasted', method = {'GET'})
@cross_origin()
def select_all_waste():
    connection = master_credentials.connect()
    cursor = connection.cursor()

    try:
        cursor.execute("SELECT * FROM wasted_ingredients;")
    except mariadb.Error:
        print(f'Unable to pull from waste table')

    data = []

    for name, waste in cursor:
        data.append({
            'ingredient_name': name,
            'stock_wasted': waste
        })

    connection.close()
    return data


@app.route('/wasted/<string:name>', method = {'GET'})
@cross_origin()
def select_waste_ingredient(name):
    connection = master_credentials.connect()
    cursor = connection.cursor()

    try:
        cursor.execute("SELECT * FROM wasted_ingredients WHERE ingredient_name = ?;",
                       (name,))
    except mariadb.Error:
        print(f'Unable to pull from waste table')

    data = []

    for name_l, waste in cursor:
        data.append({
            'ingredient_name': name_l,
            'stock_wasted': waste
        })

    connection.close()
    return data


@app.route('/employees', methods = {'GET'})
@cross_origin()
def select_all_employees():
    connection = master_credentials.connect()
    cursor = connection.cursor()

    try:
        cursor.execute("SELECT * FROM employees;")
    except mariadb.Error:
        print(f'Unable to pull from employees table')

    data = []

    for emp_id, name, wage, hours in cursor:
        data.append({
            'employee_id': emp_id,
            'employee_name': name,
            'wage': wage,
            'hours_per_week': hours
        })

    connection.close()
    return data


@app.route('/employees/<string:emp_id>', method = {'GET'})
@cross_origin()
def select_employee(emp_id):
    connection = master_credentials.connect()
    cursor = connection.cursor()

    try:
        cursor.execute("SELECT * FROM employees WHERE employee_id = ?;", (emp_id,))
    except mariadb.Error:
        print(f'Unable to pull from employees table')

    data = []

    for empl_id, name, wage, hours in cursor:
        data.append({
            'employee_id': empl_id,
            'employee_name': name,
            'wage': wage,
            'hours_per_week': hours
        })

    connection.close()
    return data


@app.route('/time_cards', method = {'GET'})
@cross_origin()
def select_all_time_cards():
    connection = master_credentials.connect()
    cursor = connection.cursor()

    try:
        cursor.execute("SELECT * FROM time_table;")
    except mariadb.Error:
        print(f'Unable to pull from time table')

    data = []

    for time_id, emp_id, time_in, time_out in cursor:
        data.append({
            'time_id': time_id,
            'employee_id': emp_id,
            'time_in': time_in,
            'time_out': time_out
        })

    connection.close()
    return data


@app.route('/time_cards/<string:emp_id>', method = {'GET'})
@cross_origin()
def select_employee_time_card(emp_id):
    connection = master_credentials.connect()
    cursor = connection.cursor()

    try:
        cursor.execute("SELECT * FROM time_table WHERE employee_id = ?;",
                       (emp_id,))
    except mariadb.Error:
        print(f'Unable to pull from time table')

    data = []

    for time_id, empl_id, time_in, time_out in cursor:
        data.append({
            'time_id': time_id,
            'employee_id': empl_id,
            'time_in': time_in,
            'time_out': time_out
        })

    connection.close()
    return data


def main():
    debug_state = True if '-d' in sys.argv or '--debug' in sys.argv else False
    app.run(host='0.0.0.0', debug=debug_state)


if __name__ == '__main__':
    main()
