import mariadb
import json


class Database:
    def __init__(self, file_path):
        file_data = open(file_path)
        data = json.load(file_data)
        file_data.close()

        self._user=file_data['user']
        self._password=file_data['password']
        self._host=file_data['host']
        self._port=file_data['port']
        self._database=file_data['database']

    @property
    def user(self):
        return self._user

    @property
    def password(self):
        return self._password

    @property
    def host(self):
        return self._host

    @property
    def port(self):
        return self._port

    @property
    def database(self):
        return self._database


    def connect(self):
        try:
            connection = mariadb.connect(
                user=self._user,
                password=self._password,
                host=self._host,
                port=self._port,
                database=self._database
            )
        except mariadb.Error as e:
            print(f"Error unable to connect: {e}")
            sys.exit(1)

        return connection


    def change_database(self, new_database):
        try:
            connection = mariadb.connect(
                user=self._user,
                password=self._password,
                host=self._host,
                port=self._port,
                database=new_database
            )

            self._database = new_database
        except mariadb.Error as e:
            print(f"Error unable to connect: {e}")
            sys.exit(1)

        return connection
