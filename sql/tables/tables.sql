CREATE TABLE ingredients (
    ingredient_name     VARCHAR(30) PRIMARY KEY,
    max_stock           INTEGER,
    current_stock       INTEGER,
    cost                NUMERIC(12,2),
    time_log            TIMESTAMP
);

CREATE TABLE recipes (
    recipe_name         VARCHAR(30) PRIMARY KEY,
    ingredient_list     VARCHAR(100),
    sell_price          NUMERIC(12,2)

);

CREATE TABLE sales (
    transaction_id      VARCHAR(30) PRIMARY KEY,
    transaction_time    DATETIME,
    product             VARCHAR(30),
    sell_price          NUMERIC(12,2),
    product_cost        NUMERIC(12,2),
    profit              NUMERIC(12,2),
    tip                 NUMERIC(12,2),
    waiter              VARCHAR(30)

);

CREATE TABLE employees (
    employee_id         VARCHAR(30) PRIMARY KEY,
    employee_name       VARCHAR(30),
    wage                NUMERIC(12,2),
    hours_per_week      INTEGER


);

CREATE TABLE time_table (
    time_id         VARCHAR(30) PRIMARY KEY,
    employee_id     VARCHAR(30),
    time_in         DATETIME,
    time_out        DATETIME,

    FOREIGN KEY (employee_id) REFERENCES employees(employee_id)
);

CREATE TABLE ingredients_used (
    ingredient_name VARCHAR(30),
    start_time      DATETIME,
    end_time        DATETIME,
    stock_used      INT,

    FOREIGN KEY (ingredient_name) REFERENCES ingredients(ingredient_name),
    PRIMARY KEY (ingredient_name, stock_used)
);

CREATE TABLE wasted_ingredients (
    ingredient_name VARCHAR(30),
    stock_wasted    INT,

    FOREIGN KEY (ingredient_name) REFERENCES ingredients(ingredient_name),
    PRIMARY KEY (ingredient_name, stock_wasted)
);

CREATE TABLE stolen goods (
    goods_stolen        VARCHAR(30) PRIMARY KEY,
    cost_estimate       NUMERIC(12,2),
    thief_description   VARCHAR(100),
    time_stamp          TIMESTAMP
    
);


INSERT INTO ingredients VALUES 
    (tomatoes, 250, 87, 15.99, '2021-05-10 08:30:00'),
    (apples, 100, 50, 12.99, '2021-05-10 08:35:00'),
    (steak, 150, 33, 25.99, '2021-05-10 08:35:00'),
    (potatoes, 500, 120, 12.99, '2021-05-10 08:30:00'),
    (eggs, 200, 37, 4.99, '2021-05-10 08:41:00');

INSERT INTO employees VALUES 
    ('A3123', 'Trent Smith', 12.50, 40),
    ('C7177', 'Alex Vorchec', 12.50, 40),
    ('E6458', 'Samantha Baren', 14.00, 40),
    ('N9991', 'Cole Reigns', 12.00, 20),
    ('A2212', 'Aaron Lord', 15.00, 32);

