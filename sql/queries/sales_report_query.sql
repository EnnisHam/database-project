# [SALES REPORT]
# Get revenue from a specified amount of time
DELIMITER !
DROP FUNCTION IF EXISTS report_revenue;
CREATE FUNCTION report_revenue(timespan VARCHAR(8)) RETURNS INTEGER
BEGIN
    RETURN (
    SELECT 
        SUM(sell_price)
    FROM
        sales
    WHERE
        transaction_time BETWEEN
        NOW() AND NOW() - INTERVAL 1 timespan
    )
END!
DELIMITER ;

# Get profit from a specified amount of time
DELIMITER !
DROP FUNCTION IF EXISTS report_profit;
CREATE FUNCTION report_profit(timespan VARCHAR(8)) RETURNS INTEGER
BEGIN
    RETURN (
    SELECT 
        SUM(profit)
    FROM
        sales
    WHERE
        transaction_time BETWEEN
        NOW() AND NOW() - INTERVAL 1 timespan
    )
END!
DELIMITER ;

# Get all sales that occurred within a certain amount of time
DELIMITER !
DROP FUNCTION IF EXISTS sales_report;
CREATE FUNCTION sales_report(timespan VARCHAR(8)) RETURNS INTEGER
BEGIN
    RETURN (
    SELECT 
        *
    FROM
        sales
    WHERE
        transaction_time BETWEEN
        NOW() AND NOW() - INTERVAL 1 timespan
    )
END!
DELIMITER ;

# Generate report, parameter can be "DAY", "WEEK", "MONTH", "YEAR"
SELECT sales_report("DAY");
SELECT report_revenue("DAY");
SELECT report_profit("DAY");
