# [INVENTORY COMPARISON]
# View change in inventory over the specified amount of time
DELIMITER !
DROP FUNCTION IF EXISTS inventory_change;
CREATE FUNCTION inventory_change(ingredient VARCHAR(50), timespan VARCHAR(8)) RETURNS INTEGER
BEGIN
    # Temporary table to store previous inventory counts
    DROP TEMPORARY TABLE IF EXISTS prev_inventory;
    CREATE TEMPORARY TABLE prev_inventory (
        ingredient_name VARCHAR(50),
        current_stock INTEGER
    );
    
    # Get stock of each ingredient from previous inventory
    # Store it in prev_inventory temporary table
    SELECT
        ingredient_name, current_stock
    INTO
        prev_inventory
    FROM
        ingredients
    WHERE
        # Get the record farthest back in the db while still in range of given
        # timespan
        timestamp BETWEEN
        NOW() AND NOW() - INTERVAL 1 timespan
        # Do it for the specified ingredient to compare
        AND ingredient_name = ingredient
    ORDER BY
        transaction_time ASC LIMIT 1
    
    # Return the difference between current inventory and prev inventory
    RETURN (
    SELECT 
        ingredient_name,
        # prev_inventory only ever has 1 row in it, just get the value with this
        current_stock - (SELECT current_stock FROM prev_inventory)
    FROM
        sales
    WHERE
        # Get the latest record in the db of the ingredient
        ingredient_name = ingredient
    ORDER BY
        transaction_time DESC LIMIT 1
    )
END!
DELIMITER ;
    
# Returns ingredient_name, and the difference between stocks from a month ago to now
SELECT inventory_change("Tomato", "MONTH");
