# [CALCULATE ANNUAL TAX]
# First, get the annual profit, then calculate tax from it
DELIMITER !
DROP FUNCTION IF EXISTS calculate_tax;
CREATE FUNCTION calculate_tax() RETURNS INTEGER
BEGIN
    DECLARE total_profit DECIMAL(16,2)
    DECLARE tax_cut DECIMAL(3,2)
    
    # Store this year's total profit
    SELECT
        report_profit("YEAR")
    INTO
        total_profit
    FROM
        sales;
        
    # Calculate what percentage of tax to pay
    # Example tax amounts
    IF (total_profit < 100000.00) THEN
        SELECT 0.15 INTO tax_cut;
    ELSE
        SELECT 0.30 INTO tax_cut;
    END IF
    
    # Calculate how much tax to pay
    RETURN(
    SELECT
        report_profit("YEAR") AS total_profit,
        total_profit * tax_cut AS tax_to_pay
    FROM
        sales
    )
END!
DELIMITER ;

SELECT calculate_tax();
