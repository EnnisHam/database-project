import { useEffect, useState } from "react";
import "./styles/app.css";
import axios from "axios";
import Sidebar from "./components/Sidebar";
import { BrowserRouter as Router, Switch, Route } from "react-router-dom";
import Recipes from "./components/main/Recipes";
import Ingredients from "./components/main/Ingredients";
import Employees from "./components/main/Employees";
import Sales from "./components/main/Sales";
import { Box } from "@material-ui/core";

interface IResponse {
  foo: string;
  input: string;
}

function App() {
  const [response, setResponse] = useState<IResponse>();

  useEffect(() => {
    // Make http requests to the two endpoints
    axios.all([axios("/foo"), axios("/bar/3")]).then(([res1, res2]) => {
      console.log(res1, res2);
      // Update state
      setResponse({ foo: res1.data.foo, input: res2.data.input });
    });
  }, []);

  return (
    <div className="app">
      <Router>
        <Sidebar />
        <Box width="100px" />
        <Switch>
          <Route path="/recipes" component={Recipes} />
          <Route path="/ingredients" component={Ingredients} />
          <Route path="/employees" component={Employees} />
          <Route path="/sales" component={Sales} />
        </Switch>
      </Router>
    </div>
  );
}

export default App;
