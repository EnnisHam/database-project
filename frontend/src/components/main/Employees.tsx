import {
  Button,
  Card,
  CardActionArea,
  CardActions,
  CardContent,
  makeStyles,
  Typography,
  Theme,
  createStyles,
} from "@material-ui/core";
import React from "react";
import "../../styles/index.css";

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: {
      maxWidth: 345,
    },
  })
);


export interface IEmployees {
  employee_id: string,
  employee_name: string,
  wage: number,
  hours_per_week: number,
}


async function fetchAllEmployees(): Promise<IEmployees[]> {
  const fetchHeaders = new Headers();

  fetchHeaders.append('Content-Type', 'application/json');

  const response = await fetch("http://localhost:5000/employees", {
    method: 'GET',
    headers: fetchHeaders,
  });

  const data = await response.json();

  if (response.ok) {
    return data;
  } else {
    return new Error("you're fucked buddy");
  }
}



function Employees() {
  const classes = useStyles();

  return (
    <div className="main">
      <h1>Employees</h1>
      <Card className={classes.root}>
        <CardActionArea>
          <CardContent>
            <Typography gutterBottom variant="h5" component="h2">
              John Robert
            </Typography>
            <Typography variant="body2" color="textSecondary" component="p">
              Lizards are a widespread group of squamate reptiles, with over
              6,000 species, ranging across all continents except Antarctica
            </Typography>
          </CardContent>
        </CardActionArea>
        <CardActions>
          <Button size="small" color="secondary">
            Remove
          </Button>
          <Button size="small" color="primary">
            Learn More
          </Button>
        </CardActions>
      </Card>
    </div>
  );
}

export default Employees;
