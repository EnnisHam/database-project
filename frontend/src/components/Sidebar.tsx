import React from "react";
import "../styles/sidebar.css";
import { Link } from "react-router-dom";
import {
  createStyles,
  Divider,
  Drawer,
  List,
  ListItem,
  ListItemIcon,
  ListItemText,
  makeStyles,
  MenuItem,
  Theme,
} from "@material-ui/core";
import FastfoodOutlinedIcon from "@material-ui/icons/FastfoodOutlined";
import AssignmentOutlinedIcon from "@material-ui/icons/AssignmentOutlined";
import PeopleAltOutlinedIcon from "@material-ui/icons/PeopleAltOutlined";
import AttachMoneyOutlinedIcon from "@material-ui/icons/AttachMoneyOutlined";

const drawerWidth = 240;

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: {
      display: "flex",
    },
    drawer: {
      width: drawerWidth,
      flexShrink: 0,
    },
    drawerPaper: {
      width: drawerWidth,
    },
    // necessary for content to be below app bar
    toolbar: theme.mixins.toolbar,
    content: {
      flexGrow: 1,
      backgroundColor: theme.palette.background.default,
      padding: theme.spacing(3),
    },
  })
);

function Sidebar() {
  const classes = useStyles();

  return (
    <div className="sidebar">
      {/* <Drawer
        className={classes.drawer}
        variant="permanent"
        classes={{
          paper: classes.drawerPaper,
        }}
        // anchor="left"
      > */}
      <div className={classes.toolbar} />
      <Divider />
      <List>
        {[
          { link: "recipes", icon: <AssignmentOutlinedIcon /> },
          { link: "ingredients", icon: <FastfoodOutlinedIcon /> },
          { link: "employees", icon: <PeopleAltOutlinedIcon /> },
          { link: "sales", icon: <AttachMoneyOutlinedIcon /> },
        ].map(({ link, icon }, index) => (
          <Link key={index} to={`/${link}`}>
            <MenuItem button>
              <ListItemIcon>{icon}</ListItemIcon>
              <ListItemText
                primary={link.charAt(0).toUpperCase() + link.slice(1)}
              />
            </MenuItem>
          </Link>
        ))}
      </List>
      <Divider />
      {/* <List>
          {["All mail", "Trash", "Spam"].map((text, index) => (
            <ListItem button key={text}>
              <ListItemIcon>
                {index % 2 === 0 ? <InboxIcon /> : <MailIcon />}
              </ListItemIcon>
              <ListItemText primary={text} />
            </ListItem>
          ))}
        </List> */}
      {/* </Drawer> */}
    </div>
  );
}

export default Sidebar;
